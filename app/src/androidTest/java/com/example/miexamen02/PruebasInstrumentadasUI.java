package com.example.miexamen02;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.miexamen02.presentacion.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.example.miexamen02.TestUtils.withRecyclerView;

// Referencia del testeo de un RecyclerView: https://github.com/dannyroa/espresso-samples
@RunWith(AndroidJUnit4.class)
public class PruebasInstrumentadasUI
{
    public static final String TEST_IDENTIFICACION = "TN001";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    // Testear despliegue de detalles correctamente mediante la verificacion del publisher del primer elemento. Así, se prueba tanto el despliegue correcto de los elementos del RecyclerView como los
    // detalles en el fragmento
    @Test
    public void testDetallesPublisher()
    {
        onView(withRecyclerView(R.id.table_top_rv).atPosition(0)).perform(click());
        onView(withId(R.id.identificacion)).check(matches(withText(TEST_IDENTIFICACION)));
    }

    // Testear despliegue de todos los elementos en la actividad principal mediante el chequeo de la existencia del ultimo elemento y la "no existencia" del elemento siguiente. Note que es normal que
    // ocurra la exception NullPointerException, y en caso de que existiera otra excepcion distinta, el test no pasaria. Ademas, si el elemento si existiese, tampoco pasaria la prueba ya que se esta
    // chequeando con el doesNotExist.
    @Test
    public void testCantidadElementos()
    {
        onView(withRecyclerView(R.id.table_top_rv).atPosition(7)).check(matches(isDisplayed()));
        try
        {
            onView(withRecyclerView(R.id.table_top_rv).atPosition(8)).check(doesNotExist());
        }
        catch (NullPointerException ignored) {}
    }


}
