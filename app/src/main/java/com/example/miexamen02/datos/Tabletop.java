package com.example.miexamen02.datos;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Tabletop implements Parcelable
{

    @NonNull
    private String identificacion;
    private String nombre;
    @SerializedName("year")
    private int anno;
    private String publisher;

    public static final String KEY = "Tabletop";

    public Tabletop(@NonNull String identificacion, String nombre, int anno, String publisher)
    {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.anno = anno;
        this.publisher = publisher;
    }

    public Tabletop(Parcel in)
    {
        this.identificacion = Objects.requireNonNull(in.readString());
        this.nombre = in.readString();
        this.anno = in.readInt();
        this.publisher = in.readString();
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setIdentificacion(@NonNull String identificacion)
    {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        dest.writeInt(anno);
        dest.writeString(publisher);
    }

    public static final Creator<Tabletop> CREATOR = new Creator<Tabletop>()
    {
        @Override
        public Tabletop createFromParcel(Parcel in) {
            return new Tabletop(in);
        }
        @Override
        public Tabletop[] newArray(int size) {
            return new Tabletop[size];
        }
    };
}