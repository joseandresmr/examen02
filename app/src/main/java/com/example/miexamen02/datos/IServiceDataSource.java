package com.example.miexamen02.datos;

import java.util.List;

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public interface IServiceDataSource
{
    void fetchItems() throws BaseDataItemsException;
    void giveItems(List<Tabletop> listaTabletops);
}