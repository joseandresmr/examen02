package com.example.miexamen02.datos;

import com.example.miexamen02.presentacion.MainActivityView;

import java.util.List;

public class TabletopInteractor implements IServiceDataSource
{
    private static final String REST_URL = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop6.json";

    private AsyncTaskRest asyncTaskRest;

    private ItemsRepository itemsRepository;

    public TabletopInteractor(ItemsRepository itemsRepository, MainActivityView mainActivityView)
    {
        this.itemsRepository = itemsRepository;
        asyncTaskRest = new AsyncTaskRest(this, mainActivityView);
    }

    @Override
    public void fetchItems()
    {
        asyncTaskRest.execute(REST_URL);
    }

    @Override
    public void giveItems(List<Tabletop> listaTabletops)
    {
        itemsRepository.giveItems(listaTabletops);
    }
}
