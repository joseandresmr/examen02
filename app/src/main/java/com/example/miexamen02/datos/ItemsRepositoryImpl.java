package com.example.miexamen02.datos;

import com.example.miexamen02.negocio.GetListItemsInteractor;
import com.example.miexamen02.presentacion.MainActivityView;

import java.util.List;

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public class ItemsRepositoryImpl implements ItemsRepository
{
    private IServiceDataSource mIServiceDataSource;
    private GetListItemsInteractor getListItemsInteractor;


    public ItemsRepositoryImpl(GetListItemsInteractor getListItemsInteractor, MainActivityView mainActivityView)
    {
        this.getListItemsInteractor = getListItemsInteractor;
        this.mIServiceDataSource = new TabletopInteractor(this, mainActivityView);
    }

    @Override
    public void giveItems(List<Tabletop> listaTabletops)
    {
        getListItemsInteractor.giveItems(listaTabletops);
    }

    @Override
    public void fetchItems() throws CantRetrieveItemsException
    {
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        // Ejemplo: base de datos local, archivos locales, archivos en la red, bases de datos en la red
        try
        {
            mIServiceDataSource.fetchItems();
        }
        catch (BaseDataItemsException e)
        {
            throw new CantRetrieveItemsException(e.getMessage());
        }
    }
}
