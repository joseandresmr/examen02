package com.example.miexamen02.datos;

import java.util.List;
// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes

// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes
public interface ItemsRepository
{
    void giveItems(List<Tabletop> listaTabletops);
    void fetchItems() throws CantRetrieveItemsException;
}