package com.example.miexamen02.presentacion;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.miexamen02.R;
import com.example.miexamen02.datos.Tabletop;


public class TabletopFragmento extends Fragment
{
    private static final String NAME_PARAM = "param1";

    private Tabletop tabletop;

    public TabletopFragmento()
    {
        // Required empty public constructor
    }

    public static TabletopFragmento newInstance(Tabletop tableTopParam) {
        TabletopFragmento fragment = new TabletopFragmento();
        Bundle args = new Bundle();
        args.putParcelable(NAME_PARAM, tableTopParam);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            tabletop = getArguments().getParcelable(NAME_PARAM);
        }
    }

    public Tabletop getTabletop()
    {
        return tabletop;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_tabletop, container,false);

        TextView nombre = view.findViewById(R.id.nombre);
        nombre.setText(tabletop.getNombre());

        TextView identificacion = view.findViewById(R.id.identificacion);
        identificacion.setText(tabletop.getIdentificacion());

        TextView anno = view.findViewById(R.id.anno);
        anno.setText(String.valueOf(tabletop.getAnno()));

        TextView publisher = view.findViewById(R.id.publisher);
        publisher.setText(tabletop.getPublisher());

        return view;
    }
}
