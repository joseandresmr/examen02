package com.example.miexamen02.presentacion;

import android.app.FragmentTransaction;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miexamen02.R;
import com.example.miexamen02.datos.Tabletop;

public class DetallesTabletopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_tabletop);

        // Obtener objeto
        Tabletop tabletop = getIntent().getParcelableExtra(Tabletop.KEY);

        final TabletopFragmento tableTopFragment = TabletopFragmento.newInstance(tabletop);

        // Transaccion para inicializar y asignar el fragmento
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, tableTopFragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
