package com.example.miexamen02.presentacion;

public class TabletopItem
{
    private String nombre;
    private String publisher;

    public TabletopItem(String nombre, String publisher) {
        this.nombre = nombre;
        this.publisher = publisher;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
