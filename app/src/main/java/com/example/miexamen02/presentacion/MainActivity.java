package com.example.miexamen02.presentacion;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miexamen02.R;
import com.example.miexamen02.datos.Tabletop;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        // interface View (V) para implementar los metodos de UI
        MainActivityView
{
    private ProgressDialog mProgressDialog;
    private MainActivityPresenter mMainActivityPresenter;

    private List<Tabletop> listaTabletops;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listaTabletops = new ArrayList<>();
        mProgressDialog = new ProgressDialog(this);
        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.action_settings)
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onDestroy()
    {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    // Esconder el indicador de progreso de la UI
    @Override
    public void hideProgress()
    {
        mProgressDialog.hide();
        recyclerView.setVisibility(View.VISIBLE);
    }
    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override
    public void setItems(List<Tabletop> items)
    {
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.table_top_rv);

        listaTabletops = items;

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView,new RecyclerItemClickListener.OnItemClickListener() {

                    @Override
                    public void onItemClick(View view, int position) {
                        eventoOnClick(position);
                    }
                })
        );

        // Performance
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        List<TabletopItem> listaItems = new ArrayList<>();
        for (Tabletop tabletop : listaTabletops)
        {
            listaItems.add(new TabletopItem(tabletop.getNombre(), tabletop.getPublisher()));
        }

        RecyclerView.Adapter adapterRV = new AdapterRV(listaItems);
        recyclerView.setAdapter(adapterRV);
    }

    @Override
    public void irDetalles(int position)
    {
        if(position < listaTabletops.size())
        {
            Intent intent = new Intent(this, DetallesTabletopActivity.class);
            Tabletop tabletop = listaTabletops.get(position);
            intent.putExtra(Tabletop.KEY, tabletop);
            startActivity(intent);
        }
    }

    @Override
    public ProgressDialog getProgressBar()
    {
        return mProgressDialog;
    }

    // Evento al dar clic en la lista
    @Override
    public void eventoOnClick(int position)
    {
        mMainActivityPresenter.onItemClicked(position);
    }
}