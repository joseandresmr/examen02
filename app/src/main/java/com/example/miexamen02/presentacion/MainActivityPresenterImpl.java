package com.example.miexamen02.presentacion;

import com.example.miexamen02.datos.Tabletop;
import com.example.miexamen02.negocio.GetListItemsInteractor;
import com.example.miexamen02.negocio.GetListItemsInteractorImpl;

import java.util.List;

// Capa de presentacion (Presenter)
// Presenter (P) -> Implementacion de MainActivityPresenter y de GetListItemsInteractor.OnFinishedListener
public class MainActivityPresenterImpl implements
        // Implementacion de MainActivityPresenter de la Capa (P)
        MainActivityPresenter,
        // La interface OnFinishedListener de GetListItemsInteractor para obtener el valor de retorno de la capa Iteractor (P)
        GetListItemsInteractor.OnFinishedListener
{
    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView)
    {
        this.mMainActivityView = mainActivityView;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor = new GetListItemsInteractorImpl(this, mMainActivityView);
    }

    @Override
    public void onResume()
    {
        if (!mGetListItemsInteractor.wasFetched() && mMainActivityView != null)
        {
            mGetListItemsInteractor.fetchItems();
        }
    }


    // Evento de clic en la lista
    @Override
    public void onItemClicked(int position)
    {
        if (mMainActivityView != null)
        {
            mMainActivityView.irDetalles(position);
        }
    }

    @Override
    public void onDestroy()
    {
        mMainActivityView = null;
    }

    // Recibo la lista de personas
    @Override
    public void onFinished(List<Tabletop> items)
    {
        if (mMainActivityView != null)
        {
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView()
    {
        return mMainActivityView;
    }
}
