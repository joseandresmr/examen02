package com.example.miexamen02.presentacion;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.miexamen02.R;

import java.util.List;

public class AdapterRV extends RecyclerView.Adapter<AdapterRV.Holder>
{
    private List<TabletopItem> list;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class Holder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView nombre;
        public TextView publisher;

        public Holder(View view) {
            super(view);
            nombre = view.findViewById(R.id.nombre);
            publisher = view.findViewById(R.id.publisher);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterRV(List<TabletopItem> data)
    {
        list = data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tabletop_item, parent, false);

        return new Holder(view);

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(Holder holder, int position)
    {
        holder.nombre.setText(list.get(position).getNombre());
        holder.publisher.setText(list.get(position).getPublisher());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return list.size();
    }
}
