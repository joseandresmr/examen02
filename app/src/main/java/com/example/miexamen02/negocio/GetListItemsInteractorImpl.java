package com.example.miexamen02.negocio;

import com.example.miexamen02.datos.CantRetrieveItemsException;
import com.example.miexamen02.datos.ItemsRepository;
import com.example.miexamen02.datos.ItemsRepositoryImpl;
import com.example.miexamen02.datos.Tabletop;
import com.example.miexamen02.presentacion.MainActivityView;

import java.util.List;

// Capa de Negocios (Presenter o Controller)
// Implementacion de GetListItemsInteractor de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter
public class GetListItemsInteractorImpl implements GetListItemsInteractor
{
    private final OnFinishedListener listener;
    private ItemsRepository mItemsRepository;
    private boolean fetched = false;

    public GetListItemsInteractorImpl(final OnFinishedListener listener, MainActivityView mainActivityView)
    {
        this.listener = listener;
        this.mItemsRepository = new ItemsRepositoryImpl(this, mainActivityView);
    }

    @Override
    public void giveItems(List<Tabletop> items)
    {
        listener.onFinished(items);
    }

    @Override
    public void fetchItems()
    {
        try
        {
            if(!fetched)
            {
                fetched = true;
                mItemsRepository.fetchItems();
            }
        }
        catch (CantRetrieveItemsException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public boolean wasFetched()
    {
        return fetched;
    }
}