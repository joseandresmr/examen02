package com.example.miexamen02.negocio;

import com.example.miexamen02.datos.Tabletop;

import java.util.List;

public interface GetListItemsInteractor
{
    interface OnFinishedListener {
        void onFinished(List<Tabletop> items);
    }
    void giveItems(List<Tabletop> items);
    void fetchItems();
    boolean wasFetched();
}
